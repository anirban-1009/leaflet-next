import Map from '@components/Map';
import styles from '@styles/Home.module.scss';

const DEFAULT_CENTER = [52.517033,13.388798];
const PATH = [
  [
    52.517033,
    13.388798
  ],
  [
    52.529432,
    13.39763
  ],
  [
    52.523239,
    13.428554
  ]
]

export default function Home() {
  return (
    <div className={styles.mapContainer}>
      <Map center={DEFAULT_CENTER} zoom={12} zoomControl={false} attributionControl={false}>
        {({ TileLayer, Marker, Popup, Polyline }) => (
          <>
            <TileLayer
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            />
            <Marker position={DEFAULT_CENTER}>
              <Popup>
                A pretty CSS3 popup. <br /> Easily customizable.
              </Popup>
            </Marker>
            <Polyline positions={PATH} />
          </>
        )}
      </Map>
    </div>
  );
}
